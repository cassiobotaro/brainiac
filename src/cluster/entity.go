package cluster

import (
	"brainiac/src/utils"
	"log"
)

type Cluster struct {
	Name   string `json:"name,omitempty" validate:"required,len=19"` // o nome do cluster possui um numero fixo de caracteres, pois é padronizado
	Region string `json:"region,omitempty" validate:"required"`
	// Status      string    `json:"status,omitempty"`
	// Doc         string    `json:"doc,omitempty"`
	// Repo        string    `json:"repo,omitempty"`
	// Environment string    `json:"environment,omitempty"`
	// CreatedAt   time.Time `json:"createdAt,omitempty"`
	// DeletedAt   time.Time `json:"deletedAt,omitempty"` // soft-deleted
	// LastSync    time.Time `json:"LastAt,omitempty"`
	// VPC         `json:"vpc,omitempty"`
	// Addons      `json:"addons,omitempty"`
	// Account `json:"account,omitempty"`
}

type Account struct {
	ID    int    `json:"id,omitempty" validate:"required,number"`
	Name  string `json:"name,omitempty" validate:"required"`
	OrgID int    `json:"orgID,omitempty" validate:"required,number"`
}

type VPC struct {
	ID    string `json:"id,omitempty" validate:"required"`
	Range string `json:"range,omitempty" validate:"required,cidrv4"`
	// SubnetPrivada []Subnet `json:"subnetPrivada,omitempty"`
	// SubnetPublica []Subnet `json:"subnetPublica,omitempty"`
}

type Subnet struct {
	ID     string `json:"id,omitempty"`
	Public bool   `json:"public,omitempty"`
	Range  string `json:"range,omitempty" validate:required,cidrv4`
}

type Addons struct {
	Name                string `json:"name,omitempty"`
	Version             int    `json:"version,omitempty"`
	SupportedVersionK8S bool   `json:"supportedVersionK8S,omitempty"`
}

// repo string, environment string, vpc VPC, addons Addons
func NewCluster(name string, region string) (*Cluster, error) {
	cluster := &Cluster{
		Name:   name,
		Region: region,
	}

	if err := utils.Validate(*cluster); err != nil {
		log.Println("error validate", err)
		return nil, err

	}

	return cluster, nil
}

// TODO: nil, fmt.Errorf("%q: %w", name, ErrUserNotFound)
