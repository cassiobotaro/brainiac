package main

import (
	"brainiac/src/entity"
	"brainiac/src/utils"
	"fmt"
	"log"
)

func main() {

	vpc := entity.VPC{"vpc-teadasd", "10.0.0.1/32"}

	if err := utils.Validate(vpc); err != nil {
		log.Fatal(err)
	}

	fmt.Println(vpc)

	c1, err := entity.NewCluster("eks-yyyxxx-use1-hom", "us-east-1")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(c1)
}
