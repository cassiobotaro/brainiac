package cluster

type Service struct {
	repo Repository
}

func (s Service) Insert(cluster Cluster) error {
	return s.repo.Insert(cluster)
}

// TODO: fazer testes de tudo que já foi implementado
