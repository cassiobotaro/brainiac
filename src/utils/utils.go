package utils

import (
	"github.com/go-playground/validator"
)

func Validate(obj any) error {
	return validator.New().Struct(obj)
}
