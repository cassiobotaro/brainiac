# Brainiac

Central de catalogs de informações de clusters.

**Motivadores**

- Informações dispersas
- Não temos uma fonte da verdade
- Deploy pra correção global de eventos críticos
- Sem modelo de referencia
- Validação de maturidade de clusters
- Consulta de recomendações e/ou boas praticas

**Objetivo do projeto:**

Visando centralizar informações e disponibilizar dados de todo o ambiente, este projeto foi idealizado para facilitar a consulta e fornecer informações de forma simples e prática.

## Entidade

### Clusters (Catalogo)

- CRUD
- Relatório geral de clusters ativos
- Relatório de cluster desativados
- Relatório com relação de versão do cluster e versão corrente de addons

Esse método terá como foco o cadastro de clusters e suas informações mais relevantes para o nosso dia a dia.

```golang
type Cluster struct {
	Name        string
	AccountName string
	Region      string
	Status      string
	Doc         string
	Repo        string
	Environment string
	CreatedAt   time.Time
	DeletedAt   time.Time // soft-deleted
	LastSync    time.Time
	VPC
	Addons
	AccountID int
}

type VPC struct {
	VPCID         string
	VPCRange      string
	SubnetPrivada []Subnet
	SubnetPublica []Subnet
}

type Subnet struct {
	SubnetID string
	Public   bool
}

type Addons struct {
	Name                string
	Version             int
	SupportedVersionK8S bool
}
```

### Catalogo de Addons

Esse método terá como foco o cadastro de addons. O objetivo é disponibilizar a consulta de versões recomendadas e padronização de versão em todo o ambiente.

```golang
type Addons struct {
  Name string
  Repo string
  WorkloadDefault string
  Version int
  SupportedVersionK8S []float
}
```

### Authentication

Seu método de autenticação será realizado através do SSO.

## System design

```mermaid
---
title: Brainiac
---
flowchart LR

BrainiacAPI-->bac[Brainiac catalogo cluster]
BrainiacAPI-->baj[Brainiac job]
BrainiacAPI-->baa[Brainiac catalog addons]

cli-->BrainiacAPI
moonlight-->BrainiacAPI
pipeline-->BrainiacAPI

BrainiacAPI-->QU
QU-->baw[Brainiac worker]
baw-- if error -->QUD
bac-->DB
baa-->DB
baj-->cluster

subgraph SVC
  DB[(Database)]
  QU[[Queue]]
  QUD[[Queue DLQ]]
end
```

---

## Casos de uso:

> Fazer tests

- Cadastrar novo cluster
- Listar clusters
- Update clusters
- Delete clusters
